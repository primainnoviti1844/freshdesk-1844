const request = require('request');
const _ = require('lodash');
const { getTicket, getApiConfig } = require('./service/FreshdeskService');
// PROD
const INNOVITI_CREATE_TICKET_HOOK = "https://freshdesk.innoviti.com:8443/fd_data_api/summary_api/index.php/createTicketData";
const INNOVITI_UPDATE_TICKET_HOOK = "https://freshdesk.innoviti.com:8443/fd_data_api/summary_api/index.php/updateTicketData";
const INNOVITI_ERROR_TICKET_HOOK = "https://freshdesk.innoviti.com:8443/fd_data_api/summary_api/index.php/logFailedTicketData";
// TEST
// const INNOVITI_CREATE_TICKET_HOOK = "https://webhook.site/ada9014a-f341-45a6-8cee-20556d374cf1";
// const INNOVITI_UPDATE_TICKET_HOOK = "https://webhook.site/ada9014a-f341-45a6-8cee-20556d374cf1";
// const INNOVITI_ERROR_TICKET_HOOK = "https://webhook.site/ada9014a-f341-45a6-8cee-20556d374cf1";

exports = {
	events: [{ event: 'onTicketCreate', callback: 'onTicketCreateHandler' },
	{ event: 'onTicketUpdate', callback: 'onTicketUpdateHandler' }],

	// args is a JSON block containing the payload information.
	// args['iparam'] will contain the installation parameter values.
	onTicketCreateHandler: function(args) {
		// console.log("onTicketCreateHandler called", JSON.stringify(args));
		this.syncTicket({ ticketId: args.data.ticket.id, iparams: args['iparams'], url: INNOVITI_CREATE_TICKET_HOOK });
	},

	onTicketUpdateHandler: function(args) {
		// console.log("onTicketUpdateHandler called", JSON.stringify(args));
		const updatedFields = Object.assign({}, args.data.ticket.changes);
		this.syncTicket({ ticketId: args.data.ticket.id, iparams: args['iparams'], url: INNOVITI_UPDATE_TICKET_HOOK, updatedFields: updatedFields });
	},

	stringifyError: function(err, filter, space) {
		var plainObject = {};
		Object.getOwnPropertyNames(err).forEach(function(key) {
			plainObject[key] = err[key];
		});
		return JSON.stringify(plainObject, filter, space);
	},

	sendError: function(errorLabel, data, err){
		console.error("Errorlabel", errorLabel, "Error", err, "data", data)
		try{
			const payload = {
				"errorLabel": errorLabel,
				"data" : data,
				"err" : this.stringifyError(err, null, '')
			}
			request.post(
				INNOVITI_ERROR_TICKET_HOOK,
				{
					body: payload,
					json: true,
				},
				function(error, response, body) {
					if (error || (response && response.statusCode >= 300)) {
						console.log('Error occurred while posting error:', JSON.stringify(error), "response", JSON.stringify(response),"body", JSON.stringify(body));
					}else{
						console.log("Error posted successfully", err);
					}
				}
			);
		}catch(err){
			console.error("Error posting failed::err", err);
			console.error("Error posting failed::data", data);
		}
	},

	syncTicket: function(args) {
		try{
			console.log("Sync ticket called");
			const isCreateEvent = args.url ? true : false;
			if (!args.url) {
				args['url'] = INNOVITI_UPDATE_TICKET_HOOK;
			}
				
			let updatedFields = args.updatedFields;
			const iparams = args.iparams;
			const updatedBy = args.updatedBy;
			// console.log('syncTicket', updatedFields, 'updatedBy:', updatedBy);

			if (updatedFields) {
				_.forEach(_.keys(updatedFields), keyx => {
					updatedFields[keyx] = Array.isArray(updatedFields[keyx]) ? updatedFields[keyx][1] : updatedFields[keyx];
				});
			}
			const ticketId = args.ticketId;
			let auth = {
				username: iparams['InnovitiUsername'],
				password: iparams['InnovitiPassword'],
			};
			const apiConfig = getApiConfig(iparams, "freshdesk");
			console.log("Fetch ticket details for ticketId", ticketId, "{ include: 'requester,stats' }");
			let ticketPromise = getTicket(apiConfig, ticketId, { include: 'requester,stats' });
			ticketPromise.then(data => {
				let payload = {};
				payload.ticket = data;
				payload.ticket.updatedFields = updatedFields;
				if (updatedBy) {
					payload.ticket.updaterDetails = {
						name: updatedBy.name,
						email: updatedBy.email,
					};
				}
				request.post(
					args.url,
					{
						body: payload,
						auth: auth,
						json: true,
					},
					function(error, response, body) {
						if (error || (response && response.statusCode >= 300)) {
							console.error('Error occurred:', JSON.stringify(error), "response", JSON.stringify(response),"body", JSON.stringify(body));
							this.sendError("Error sending data to innoviti", {"payload": payload, "response": JSON.stringify(response), "body": JSON.stringify(body)} , JSON.stringify(error))
						}else if(!isCreateEvent){
							console.log("renderData called");
							renderData();
						}
					}
				);
			})
			.catch(err=>{
				this.sendError("Error occurred while fetching ticket data", {args: args, ticketId: ticketId, }, err)
			});
		}
		catch(err){
			this.sendError("Error occurred while pushing data", args, err)
		}
		
	},
};
