const request = require('request');
const Buffer = require('buffer').Buffer;
const _ = require('lodash');

const FreshdeskService = {
	/**
	 * Function to generate api token from api key
	 */
	getAuthHeaders: function(apiKey) {
		let auth = 'Basic ' + Buffer.from(apiKey + ':' + 'X').toString('base64');
		return auth;
	},

	/**
	 * Function to make api request to Freshdesk V2 apis. Support JSON response currently
	 */
	makeRequest: function(method, uri, apiConfig, qs, data) {
		const authToken = FreshdeskService.getAuthHeaders(apiConfig.apiKey);
		const url = `${apiConfig.serverName}${uri}`;
		return new Promise(function(resolve, reject) {
			const options = {
				method: method,
				headers: {
					'Content-Type': 'application/json',
					Authorization: authToken,
				},
				url: url,
				qs: qs,
			};
			if (data) {
				options.body = JSON.stringify(data);
			}
			console.log("FreshdeskService.makeRequest options", options);
			request(options, function(error, response, body) {
				if (error) {
					return reject(error);
				}
				if (response.statusCode >= 300) {
					return reject({statusCode: response.statusCode, requestOptions: options, body: body});
				}
				return resolve(JSON.parse(body)); // assuming response would always be JSON
			});
		});
	},

	/**
	 * Function to update ticket fields in Freshdesk
	 */
	updateTicket: function(apiConfig, ticketId, updateObject) {
		return new Promise((resolve, reject) => {
			FreshdeskService.makeRequest('PUT', `/api/v2/tickets/${ticketId}`, apiConfig, null, updateObject)
				.then(data => {
					console.log('Ticket updated successfully', data);
					return resolve(true);
				})
				.catch(err => {
					console.log('error updating ticket', err);
					return reject(false);
				});
		});
	},

	getTickets: function(apiConfig, filters) {
		return new Promise((resolve, reject) => {
			FreshdeskService.makeRequest('GET', `/api/v2/tickets`, apiConfig, filters, null)
				.then(data => {
					console.log('Tickets fetched successfully');
					return resolve(data);
				})
				.catch(err => {
					console.log('error get ticket', err);
					return reject(null);
				});
		});
	},

	getTicket: function(apiConfig, ticketId, qs = null) {
		return new Promise((resolve, reject) => {
			FreshdeskService.makeRequest('GET', `/api/v2/tickets/${ticketId}`, apiConfig, qs, null)
				.then(data => {
					console.log('Ticket fetched successfully');
					return resolve(data);
				})
				.catch(err => {
					console.log('error get ticket', err);
					return reject(err);
				});
		});
	},

	/**
	 * Helper function to pass on only Freshdesk API configs from iparams
	 */
	getApiConfig: function(iparams, freshWorkApp = 'freshservice') {
		if (!(iparams && iparams.subdomain && iparams.apiKey)) {
			console.log('Invalid iparams', iparams);
			return null;
		}
		return {
			serverName: `https://${iparams.subdomain}.${freshWorkApp}.com`,
			apiKey: iparams.apiKey,
		};
	},

	createMapping: function(key, value) {
		return new Promise((resolve, reject) => {
			$db.set(key, { value: value })
				.done(data => {
					return resolve(data);
				})
				.fail(err => {
					console.log('error creating mapping', err);
					return reject(false);
				});
		});
	},

	/**
	 * Get value stored in DB by key. If doesn't exist, it returns null.
	 */
	getValueByKey: function(key) {
		console.log('getValueByKey', key);
		return new Promise((resolve, reject) => {
			$db.get(key)
				.done(data => {
					console.log('Mapping fetch for key', key, 'value', data);
					return resolve(data.value);
				})
				.fail(err => {
					if (err.status == 404) {
						return resolve(null);
					} else {
						console.log('Error occurred while fetching value for key', key, err);
						return reject(err);
					}
				});
		});
	},

	/**
	 * Function to delete a key and its data
	 */
	deleteMapping: function(key) {
		return new Promise((resolve, reject) => {
			$db.delete(key)
				.done(() => {
					return resolve();
				})
				.fail(err => {
					if (err.status == 404) {
						return resolve();
					} else {
						console.log('Error deleting mapping', err);
						return reject(err);
					}
				});
		});
	},

	createScheduleObject: (name, scheduleAt, data, repeatTimeUnit = null, repeatFrequency = null) => {
		let schedule = { name, schedule_at: scheduleAt, data };

		let allowedTimeUnits = ['minutes', 'hours', 'days'];
		if (repeatTimeUnit && repeatFrequency && _.includes(allowedTimeUnits, repeatTimeUnit)) {
			schedule['repeat'] = {
				time_unit: repeatTimeUnit,
				frequency: repeatFrequency,
			};
		}
		return schedule;
	},

	createSchedule: scheduleObject => {
		return new Promise((resolve, reject) => {
			$schedule.create(scheduleObject).then(
				function(data) {
					return resolve(data);
				},
				function(err) {
					console.log('error creating shedule', err.status, err.message);
					return reject(err);
				}
			);
		});
	},

	getSchedule: scheduleName => {
		return new Promise((resolve, reject) => {
			$schedule.fetch({ name: scheduleName }).then(
				function(data) {
					return resolve(data);
				},
				function(err) {
					console.log('error fetching shedule', err.status, err.message);
					return reject(err);
				}
			);
		});
	},

	updateSchedule: scheduleObject => {
		return new Promise((resolve, reject) => {
			$schedule.update(scheduleObject).then(
				function(data) {
					return resolve(data);
				},
				function(err) {
					console.log('error updating shedule', err.status, err.message);
					return reject(err);
				}
			);
		});
	},

	deleteSchedule: scheduleName => {
		return new Promise((resolve, reject) => {
			$schedule.delete({ name: scheduleName }).then(
				function(data) {
					return resolve(data);
				},
				function(err) {
					console.log('error deleting shedule', err.status, err.message);
					return reject(err);
				}
			);
		});
	},

	getAsset: (displayId, apiConfig) => {
		return new Promise((resolve, reject) => {
			FreshdeskService.makeRequest('GET', `/api/v2/assets/${displayId}`, apiConfig, null, null)
				.then(data => {
					console.log('Assets fetched successfully');
					return resolve(data);
				})
				.catch(err => {
					console.log('error getting asset', err);
					return reject(null);
				});
		});
	},

	getAgents: (apiConfig) => {
		return new Promise((resolve, reject) => {
			FreshdeskService.makeRequest('GET', `/api/v2/agents/`, apiConfig, null, null)
				.then(data => {
					console.log('Agents fetched successfully');
					return resolve(data);
				})
				.catch(err => {
					console.log('error getting agents', err);
					return reject(null);
				});
		});
	},

	getAgent: (agentId, apiConfig) => {
		return new Promise((resolve, reject) => {
			FreshdeskService.makeRequest('GET', `/api/v2/agents/${agentId}`, apiConfig, null, null)
				.then(data => {
					console.log('Agent fetched successfully');
					return resolve(data);
				})
				.catch(err => {
					console.log('error getting agent', err);
					return reject(null);
				});
		});
	},
};

exports = FreshdeskService;
// module.exports = FreshdeskService;
