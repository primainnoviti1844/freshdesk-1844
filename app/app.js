$(document).ready(function() {
	app.initialized().then(function(_client) {
		var client = _client;
		client.events.on('ticket.propertiesUpdated', function(event) {
			client.data
				.get('ticket')
				.then(function(data) {
					var event_data = event.helper.getData();
					if(Object.keys(event_data.changedAttributes).length === 1 
						&& Object.keys(event_data.changedAttributes[0] === "isCustomFieldsChanged")){ // trigger only when custom fields are updated
						client.data.get('loggedInUser').then(userData => {
							let agent = {};
							if(userData && userData.loggedInUser && userData.loggedInUser.contact){
								agent['name'] = userData.loggedInUser.contact.name;
								agent['email'] = userData.loggedInUser.contact.email;
							}
							let args = {
								ticketId: data.ticket.id,
								updatedFields: event_data.changedAttributes,
								updatedBy: agent,
							};
							client.request.invoke('syncTicket', args).then(
								function() {
									console.log("syncTIcket success");
								},
								function(err) {
									console.log("Error occurred while invoking syncTicket",JSON.stringify(err));
								}
							);
						});
					}else{
						console.log("Not a pure custom field update");	
					}
				})
				.catch(function(e) {
					console.log('Unknown error occurred', JSON.stringify(e));
				});
				
		});
	});
});
